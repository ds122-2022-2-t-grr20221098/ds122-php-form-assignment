<?php
require "check_form.php"; ?>
<!DOCTYPE html>
<html>

<head>
    <title>Teste PHP</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="check_form.js"></script>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="page-header">Teste Formulário PHP</h1>

                <?php if ($_SERVER["REQUEST_METHOD"] == "POST"): ?>
                <?php if (!$erro): ?>
                <div class="alert alert-success">
                    Dados recebidos com sucesso:
                    <ul>
                        <li><strong>Nome</strong>: <?php echo $nome; ?>;</li>
                        <li><strong>Email</strong>: <?php echo $email; ?>;</li>
                        <li><strong>Data de nascimento</strong>: <?php echo $data; ?>;</li>
                        <li><strong>Senha</strong>: <?php echo $password; ?>;</li>
                        <li><strong>Confirmação de senha</strong>: <?php echo $passwordconf; ?>;</li>
                        <li><strong>Imagem para o usuário</strong>: <?php echo $image; ?>;</li>
                        <?php // limpa o formulário.
                        $nome = "";
                        $email = "";
                        $password = "";
                        $passwordconf = "";
                        $data = "";
                        $image = "";
                        ?>
                    </ul>
                </div>
                <?php else: ?>
                <div class="alert alert-danger">
                    Erros no formulário.
                </div>
                <?php endif; ?>
                <?php endif; ?>

                <form enctype="multipart/form-data" id="form-test" class="form-horizontal" method="POST" action="<?php echo htmlspecialchars(
                        $_SERVER["PHP_SELF"]
                    ); ?>">

                    <div class="form-group <?php if (!empty($erro_nome)) {
                        echo "has-error";
                    } ?>">
                        <label for="inputNome" class="col-sm-3-lg-12 control-label">Nome</label>
                        <div class="col-sm-2-lg-6">
                            <input required type="text" class="form-control" name="nome" placeholder="Nome"
                                value="<?php echo $nome; ?>">
                            <div id="erro-nome">
                            </div>
                            <?php if (!empty($erro_email)): ?>
                            <span class="help-block"><?php echo $erro_email; ?></span>
                            <?php endif; ?>
                            <label for="inputEmail" class="col-sm-3-lg-12 control-label">Email</label>
                            <div class="col-sm-2-lg-6">
                                <input required type="email" class="form-control" name="email" placeholder="Email"
                                    value="<?php echo $email; ?>">
                                <div id="erro-email">
                                </div>
                                <?php if (!empty($erro_data)): ?>
                                <span class="help-block"><?php echo $erro_data; ?></span>
                                <?php endif; ?>
                                <label for="inputData" class="col-sm-3-lg-12 control-label">Data de nascimento::</label>
                                <div class="col-sm-2-lg-6">
                                    <input required type="data" class="form-control" name="data" placeholder="Data"
                                        value="<?php echo $data; ?>">
                                    <div id="erro-data">
                                    </div>
                                    <?php if (!empty($erro_password)): ?>
                                    <span class="help-block"><?php echo $erro_password; ?></span>
                                    <?php endif; ?>
                                    <label for="inputPassword" class="col-sm-3-lg-12 control-label">Senha:</label>
                                    <div class="col-sm-2-lg-6">
                                        <input required type="password" class="form-control" name="password"
                                            placeholder="Password" value="<?php echo $password; ?>">
                                        <div id="erro-password">
                                        </div>
                                        <?php if (
                                            !empty($erro_passwordconf)
                                        ): ?>
                                        <span class="help-block"><?php echo $erro_passwordconf; ?></span>
                                        <?php endif; ?>
                                        <label for="inputPasswordConf" class="col-sm-3-lg-12 control-label">Senha de
                                            confirmação: </label>
                                        <div class="col-sm-2-lg-6">
                                            <input required type="password" class="form-control" name="passwordconf"
                                                placeholder="PasswordConf" value="<?php echo $passwordconf; ?>">
                                            <div id="erro-passwordconf">
                                            </div>
                                            <?php if (!empty($erro_image)): ?>
                                            <span class="help-block"><?php echo $erro_image; ?></span>
                                            <?php endif; ?>
                                            <label for="inputImage" class="col-sm-3-lg-12 control-label">Upload de
                                                imagem:
                                            </label>
                                            <div class="col-sm-2-lg-6">
                                                <input required type="file" class="form-control" name="image"
                                                    placeholder="image" value="<?php echo $image; ?>">
                                                <div id="erro-image">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <div class="col-sm-2-lg-6">
                                            <button type="submit" class="btn btn-default">Enviar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

</html>