nome_input = $("input[name='nome']");
email_input = $("input[name='email']");
password_input = $("input[name='password']");
passwordconf_input = $("input[name='passwordconf']");
data_input = $("input[name ='data']");
image_input = $("input[name='image']");

$(function () {
  $("#form-test").on("submit", function () {
    if (nome_input.val() == "" || nome_input.val() == null) {
      $("#erro-nome").html("O nome é obrigatorio");
      return false;
    }
    if (email_input.val() == "" || email_input.val() == null) {
      $("#erro-email").html("O email é obrigatório");
      return false;
    }
    if (password_input.val() == "" || password_input.val() == null) {
      $("#erro-password").html("A senha é obrigatório");
      return false;
    }
    if (data_input.val() == "" || data_input.val() == null) {
      $("#erro-data").html("A data de nascimento é obrigatório");
      return false;
    }
    if (image_input.val() == "" || image_input.val() == null) {
      $("#erro-image").html("O envio da imagem é obrigatório");
      return false;
    }
    return true;
  });
});
