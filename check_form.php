<?php
function verifica_campo($texto){
  $texto = trim($texto);
  $texto = stripslashes($texto);
  $texto = htmlspecialchars($texto);
  return $texto;
}

$nome = "";
$email = "";
$password = "";
$passwordconf = "";
$data = "";
$image = ""; 
$erro = false;

if ($_SERVER["REQUEST_METHOD"] == "POST") {

  if(empty($_POST["nome"])){
    $erro_nome = "Nome é obrigatório.";
    $erro = true;
  }
  else{
    $nome = verifica_campo($_POST["nome"]);
  }
   if(empty($_POST["email"])){
    $erro_email = "Email é obrigatório.";
    $erro = true;
  }
  else{
    $email = verifica_campo($_POST["email"]);
  }
   if(empty($_POST["password"])){
    $erro_password = "Senha é obrigatório.";
    $erro = true;
  }
  else{
    $password = verifica_campo($_POST["password"]);
  }
   if(empty($_POST["data"])){
    $erro_data = "Data de nascimento é obrigatório.";
    $erro = true;
  }
  else{
    $data = verifica_campo($_POST["data"]);
  }
   if(empty($_POST["passwordconf"])){
    $erro_passwordconf = "Confirmação de senha é obrigatório.";
    $erro = true;
  }
  else{
    $passwordconf = verifica_campo($_POST["passwordconf"]);
  }
    if(empty($_POST["image"])){
    $erro_image = "Envio de imagem é obrigatório.";
    $erro = true;
  }
  else{
    $image = verifica_campo($_POST["image"]);
  }
}
?>